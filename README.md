# Microservices discovery server

This project contains API microservices written in Java using
[Spring Boot](https://projects.spring.io/spring-boot/) and built
using [Maven](https://maven.apache.org/).
